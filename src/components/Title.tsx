import React, { FC } from 'react';

type Prop = {
  text: string;
};

const Title: FC<Prop> = ({ text }) => (
  <div className="titleContainer">
    <text className="title">{text}</text>
  </div>
);

Title.defaultProps = {
  text: '',
};

export default Title;
